//Import Pages
import AboutUs from './pages/AboutUs'
import GlobalStyle from './components/GlobalStyle'
import Nav from './components/Nav';
import ContactUs from './pages/ContactUs'
import OurWork from './pages/OurWork'
import { Switch, Route } from 'react-router-dom'
function App() {
    return (
        <div className="App">
            <GlobalStyle />
            <Nav />
            <Switch>
                <Route path="/" exact>
                    <AboutUs />
                </Route>
                <Route path="/work" exact>
                    <OurWork />
                </Route>
                <Route path="/contact" exact>
                    <ContactUs />
                </Route>
            </Switch>
        </div>
    );
}

export default App;
